using libtisiwebdll.Factory;
using mkcs.libtisiweb.Repositories;
using NUnit.Framework;

namespace mkcs.libtisiwebtest {

	public class FragmentRepositoryTest {

		public FragmentRepositoryTest () {
			UUT.DefaultSubset = "en";
		}

		private IFragmentRepository UUT = new FragmentRepository(DefaultFactory.GetFactory());

        [Test()]
        public void T0_ErrorMarker_FragmentStoreEmpty()
        {
            Assert.AreEqual(FragmentRepository.ErrorMarker_FragmentStoreEmpty, UUT.GetFragmentValue("foo.bar", "en"));
        }

        [Test()]
		public void T1_SetFragmentValue()
        {
			UUT.SetFragmentValue("navigation.about", "en", "About");
			UUT.SetFragmentValue("navigation.about", "de", "Über");
			UUT.SetFragmentValue("welcome.text", "en", "Welcome on this page");
            UUT.SetFragmentValue("welcome.title", "yy", "This is for T5_ErrorMarker_SubsetNotFound");
            UUT.SetFragmentValue("general.copyright", "", "(C) me, 2015"); // language independent
			Assert.AreEqual(4, UUT.Count);
		}

		[Test()]
		public void T2_GetFragmentValue()
        {
			Assert.AreEqual("About", UUT.GetFragmentValue("navigation.about", "en"));
			Assert.AreEqual("Über", UUT.GetFragmentValue("navigation.about", "de"));
			Assert.AreEqual("Welcome on this page", UUT.GetFragmentValue("welcome.text", "en"));
		}

		[Test()]
		public void T3_GetFragmentValue()
        {
			Assert.AreEqual("Welcome on this page", UUT.GetFragmentValue("welcome.text", "de")); // test fall back to DefaultSubset
		}
		
		[Test()]
		public void T4_GetFragmentValue()
        {
			// Test if language independent queries work
			Assert.AreEqual("(C) me, 2015", UUT.GetFragmentValue("general.copyright", "de"));
			Assert.AreEqual("(C) me, 2015", UUT.GetFragmentValue("general.copyright", "en"));
        }

        [Test()]
        public void T5_ErrorMarker_SubsetNotFound()
        {
            var expected = FragmentRepository.ErrorMarker_SubsetNotFound.Replace("$", "('welcome.title','en')");
            var actual = UUT.GetFragmentValue("welcome.title", "xx");
            Assert.AreEqual(expected, actual);
        }

        [Test()]
        public void T6_ErrorMarker_FragmentNotFound()
        {
            var expected = FragmentRepository.ErrorMarker_FragmentNotFound.Replace("$", "('not.here')");
            var actual = UUT.GetFragmentValue("not.here", "xx");
            Assert.AreEqual(expected, actual);
        }
    }
}