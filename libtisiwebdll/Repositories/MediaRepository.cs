/*
 * *ti*ny *si*mple web management system
 * (C) Michael Kremser, 2003-2023
 * 
 * This is free software.
 * License: MIT
*/

using System;
using System.IO;
using System.Collections.Generic;
using mkcs.libtisiweb.Enums;
using mkcs.libtisiweb.Models;
using System.Linq;
using libtisiwebdll.Factory;

namespace mkcs.libtisiweb.Repositories
{
	public class MediaRepository
    {
		public MediaRepository(IFactory factory)
        {
            this.factory = factory ?? throw new ArgumentNullException(nameof(factory));
        }

        private readonly Dictionary<MediaType, string[]> MediaTypeExtensions = new Dictionary<MediaType, string[]>
        {
            [MediaType.Picture] = new[] { "jpg", "jpeg", "png", "gif" },
            [MediaType.Audio] = new[] { "ogg", "wav", "mp3", "flac", "m4a" },
            [MediaType.Video] = new[] { "avi", "mpeg", "mp4", "ogv" }
        };
        private readonly IFactory factory;

        public List<Media> ReadMediaListFromPath(DirectoryInfo MediaDirectory, DirectoryInfo MediaThumbs, DirectoryInfo MediaMidRes, MediaType MediaTypeToSearchFor)
        {
			if (MediaDirectory == null)
				throw new ArgumentNullException(nameof(MediaDirectory));
			if (!MediaTypeExtensions.ContainsKey(MediaTypeToSearchFor))
				throw new ArgumentException("The requested media type cannot be searched for.");

            var searchPatterns = MediaTypeExtensions[MediaTypeToSearchFor];

            var mediaList = new List<Media>();
			Media media;
            var filesMatchingSearchPattern = new List<FileInfo>();
            foreach (var searchPattern in searchPatterns)
            {
                filesMatchingSearchPattern.AddRange(MediaDirectory.GetFiles().Where(fi => fi.Name.ToLowerInvariant().EndsWith(searchPattern, StringComparison.InvariantCultureIgnoreCase)));
            }
            foreach (FileInfo fileInfo in filesMatchingSearchPattern) // specifing searchPattern in GetFiles causes problems related to case-sensitivity on earnest operating systems
            {
                media = new Media
                {
                    Modified = fileInfo.LastWriteTime,
                    ID = fileInfo.Name,
                    Description = Fragment.CreateFragment(factory)
                };
                media.Description.Name = fileInfo.Name;
                media.Description.AddSubset("", $"{MediaTypeToSearchFor} {fileInfo.Name}");
                mediaList.Add(media);
            }
			mediaList.Sort((x, y) => x.Modified.CompareTo(y.Modified));
			return mediaList;
		}
	}
}

