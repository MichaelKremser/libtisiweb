/*
 * *ti*ny *si*mple web management system
 * (C) Michael Kremser, 2003-2023
 * 
 * This is free software.
 * License: MIT
*/

namespace mkcs.libtisiweb.Repositories
{

	public interface IFragmentRepositoryReader<T>
    {
		void ReadFragmentRepository(T repository, IFragmentRepository fragmentRepository);
	}
}