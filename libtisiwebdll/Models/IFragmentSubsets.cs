﻿using System.Collections.Generic;

namespace mkcs.libtisiweb.Models
{
    public interface IFragmentSubsets : IDictionary<string, string>
    {
        IFragment OwningFragment { get; set; }
    }
}