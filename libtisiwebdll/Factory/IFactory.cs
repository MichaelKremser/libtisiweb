﻿using mkcs.libtisiweb.Models;

namespace libtisiwebdll.Factory
{
    public interface IFactory
    {
        IFragmentSubsets CreateFragmentSubsets(IFragment owningFragment);
    }
}
