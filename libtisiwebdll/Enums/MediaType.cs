/*
 * *ti*ny *si*mple web management system
 * (C) Michael Kremser, 2003-2023
 * 
 * This is free software.
 * License: MIT
*/

namespace mkcs.libtisiweb.Enums
{

	public enum MediaType {
		Unknown = 0,
		Picture = 1,
		Video = 2,
		Audio = 3
	}
}